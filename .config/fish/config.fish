        ##################################################### IMPORTS  #############################################################

source $HOME/.config/fish/functions/cd_using_fzf.fish
source $HOME/.config/fish/functions/file_extractor.fish
source $HOME/.config/fish/functions/vi_using_fzf.fish
source $HOME/.config/fish/functions/qr_generator.fish
source $HOME/.config/fish/functions/first_touch_then_vi.fish

        ##################################################### PATH VARIABLES  #############################################################

export PATH="$HOME/.local/bin/:$PATH"
export PATH="$HOME/.config/suckless/dmenu/scripts/:$PATH"
export PATH="$HOME/.cargo/bin/:$PATH"
export PATH="$HOME/Tools/yazi/target/release:$PATH"
export PATH="$HOME/.config/emacs/bin/:$PATH"
export FZF_DEFAULT_OPTS='--height 70% --layout=reverse --border rounded'
export EDITOR='vi'
export YAZI_CONFIG_HOME="$HOME/.config/yazi/"


        ##################################################### KEYBINDINGS #############################################################

bind \ce 'vi_using_fzf; commandline -f repaint'
bind \cp 'fish --private ; commandline -f repaint'
bind \co 'cd_using_fzf; commandline -f repaint'

        ##################################################### ALIASES #############################################################

alias fso='source $HOME/.config/fish/config.fish'
alias svi='sudoedit'
alias sl='cd ~/.config/suckless/slstatus/'
alias ls='exa -l -g --icons'
alias ll='ls --tree --level=2 -a'

alias gstat='git status'
alias gpsh='git push origin master'
alias gcmt='git commit'
alias gad='git add'

alias plst='pacman -Qq | fzf --preview "pacman -Qil {}" --bind "enter:execute(pacman -Qil {} | less)"'
alias pfind='pacman -Ss'
alias batstatus='upower -i /org/freedesktop/UPower/devices/battery_BAT0'
alias mkdir='mkdir -p'
alias sync='sudo pacman -Sy'
alias tls='tmux ls'
alias tach='tmux attach'
alias update='lastUpdate.sh && sudo pacman -Syu && xmonad --recompile && upDate.sh'
alias install='sudo pacman -S'
alias remove='sudo pacman -Rns'
alias pclean='remove $(pacman -Qtdq)'
alias clean='sudo pacman -Sc'

starship init fish | source

        ##################################################### GENERAL OPTIONS  #############################################################

set -U fish_greeting ""
set -U fish_features qmark-noglob

