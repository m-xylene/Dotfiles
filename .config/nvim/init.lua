local nvim_path = vim.fn.expand("$HOME") .. "/.config/nvim/lua/"

dofile(nvim_path .. "theNeovim/options.lua")
dofile(nvim_path .. "theNeovim/keybindings.lua")
dofile(nvim_path .. "theNeovim/autocmds.lua")

require("plugins.launch")

spec("plugins.lsp")
spec("plugins.cmp")
spec("plugins.telescope")
spec("plugins.treesitter")
spec("plugins.liveServer")
spec("plugins.color")
spec("plugins.null-ls")
spec("plugins.startup")
spec("plugins.trouble")

dofile(nvim_path .. "plugins/lazy.lua")
