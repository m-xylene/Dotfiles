--ColorScheme
local M = {
	"ellisonleao/gruvbox.nvim",
	event = "UIEnter",
}

function M.config()
	require("gruvbox").setup({
		contrast = "hard",
		dim_inactive = true,
		transparent_mode = true,
		overrides = {
			SignColumn = { bg = "none" },
			Float = { bg = "none" },
			Pmenu = { bg = "none" },
			NormalNC = { bg = "none" },
		},
	})
	vim.cmd("colorscheme gruvbox")
end

function ColorMyPencil() end

return M
