local opts = { silent = true }

vim.keymap.set("n", "<leader>L", ":Lazy<CR>", opts)

vim.keymap.set("n", "<leader>M", ":Mason<CR>", opts)

vim.keymap.set("n", "<leader>htm", ":-1read $HOME/.vi/snippets/.skeleton.htm<CR> 6jvit", opts)
vim.keymap.set("n", "<leader>cpp", ":-1read $HOME/.vi/snippets/.skeleton.cpp<CR> 3jcc", opts)

vim.keymap.set("n", "<leader>gpp", ":belowright split term://g++ % -o %:r && ./%:r<CR>", opts)

vim.keymap.set("n", "x", '"_x')

vim.keymap.set("n", "s", '"_s')

vim.keymap.set("n", "J", "mzJ`z")

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

vim.keymap.set("n", "<leader>bk", ":bw<Return>", opts)

vim.keymap.set("n", "<leader>h", "<C-w>h")
vim.keymap.set("n", "<leader>j", "<C-w>j")
vim.keymap.set("n", "<leader>k", "<C-w>k")
vim.keymap.set("n", "<leader>l", "<C-w>l")

vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
