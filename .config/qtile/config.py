import os
import subprocess

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group,Screen,Key
from qtile_extras import widget

from libqtile.lazy import lazy
from libqtile import hook, bar

import colors
colors, backgroundColor, foregroundColor, workspaceColor, chordColor = colors.gruvbox()

mod = "mod1"
terminal = "kitty"

keys = [
    Key([mod], "l", lazy.layout.next(), desc="Switch focus"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "c", lazy.window.kill(), desc="Kill focused window"),
    Key([mod], "q", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod, "shift"], "space", lazy.next_layout(), desc="Toggle between layouts"),
]

groups = []
group_names = ["1", "2", "3", "4", "5", "6", "7", "8"]
group_labels = ["1", "2", "3", "4", "5", "6", "7", "8"]
group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall"]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))
 
for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=False),
                desc="Move focused window to group {}".format(i.name),
            ),
        ]
    )

layouts = [
    layout.MonadTall(
        margin=5,
        border_width=1,
        border_focus="#fe8019",
        border_normal="#000000"
    ),
    layout.Max()
]


widget_defaults = dict(
    font="JetBrains Mono Nerd Font",
    fontsize=14,
    padding=2,
    background="#1d2021"
)


def init_widgets_list(monitor_num):
    widgets_list = [
        widget.GroupBox(
            font="JetBrainsMono Nerd Font",
            fontsize=14,
            margin_y=2,
            margin_x=2,
            padding_y=6,
            padding_x=6,
            borderwidth=2,
            toggle=False,
            disable_drag=True,
            active=colors[2],
            inactive=colors[1],
            hide_unused=True,
            rounded=False,
            highlight_method="line",
            highlight_color=[backgroundColor, backgroundColor],
            this_current_screen_border=colors[10],
            this_screen_border=colors[7],
            other_screen_border=colors[6],
            other_current_screen_border=colors[6],
            urgent_alert_method="line",
            urgent_border=colors[9],
            urgent_text=colors[1],
            foreground=foregroundColor,
            background=colors[0],
            use_mouse_wheel=False
        ),

        widget.Spacer(length = bar.STRETCH),

        widget.TextBox(text=" ", fontsize=16,
                       font="Font Awesome 6 Free Solid", padding=0, foreground=colors[7]),

        widget.CPU(
            font="JetBrainsMono Nerd Font",
            update_interval=2,
            format='{load_percent}%',
            foreground=foregroundColor,
            padding=2
        ),

        widget.Sep(linewidth=0, padding=15),

        widget.TextBox(text="", padding=0, fontsize=16,
                       font="Font Awesome 6 Free Solid", foreground=colors[3]),

        widget.Memory(
            font="JetBrainsMonoNerdFont",
            foreground=foregroundColor,
            format='{MemUsed: .0f}{mm}',
            measure_mem='M',
            padding=0,
            update_interval=2
        ),
        widget.Sep(linewidth=0, padding=15),

        widget.TextBox(text="", fontsize=19, padding=0,
                       font="Font Awesome 6 Free Solid", foreground=colors[11]),

        widget.Battery(
            format='{percent:2.0%}{char}',
            font="JetBrainsMono Nerd Font",
            padding=3,
            foreground=foregroundColor,
            full_char='(o)',
            charge_char='(+)',
            discharge_char='(-)',
            unknown_char='(?)',
            update_interval=2,
            show_short_text = False,
        ),

        widget.Sep(linewidth=0, padding=8),

        widget.TextBox(text="", fontsize=16, padding=0,
                       font="Font Awesome 6 Free Solid", foreground=colors[5]),

        widget.Volume(padding=3, foreground=foregroundColor, update_interval=0.1),

        widget.Sep(linewidth=0, padding=15),

        widget.TextBox(text="", fontsize=16, padding=0,
                       font="Font Awesome 6 Free Solid", foreground=colors[10]),

        widget.Clock(format='%d-%a-(%H:%M)', font="JetBrainsMono Nerd Font",
                     padding=5, foreground=foregroundColor),

        widget.Systray(background=backgroundColor, icon_size=30, padding=0),
    ]

    return widgets_list

extension_defaults = widget_defaults.copy()

widgets_list = init_widgets_list("1")
screens = [
    Screen(top=bar.Bar(widgets=widgets_list, size=23,
           background=backgroundColor, margin=0, opacity=1),),
]

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_app_rules = []  # type: list
follow_mouse_focus = False
bring_front_click = False
cursor_warp = False

auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True
auto_minimize = True
wl_input_rules = None


@lazy.function
@hook.subscribe.startup
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.Popen([home])

wmname = "Qtile"
